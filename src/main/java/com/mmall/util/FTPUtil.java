package com.mmall.util;

import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * 文件上传到FTP服务器上
 * Created by 17936 on 2017/8/22.
 */
public class FTPUtil {

    private static  final Logger logger= LoggerFactory.getLogger(FTPUtil.class);

    private  static String ftpIp=PropertiesUtil.getProperty("ftp.server.ip");
    private  static  String ftpUser=PropertiesUtil.getProperty("ftp.user");
    private static String ftpPass=PropertiesUtil.getProperty("ftp.pass");

    public FTPUtil(String ip,int port,String user,String pwd){
        this.ip=ip;
        this.port=port;
        this.user=user;
        this.pwd=pwd;
    }
    //上传图片
    public static boolean uploadFile(List<File> fileList) throws IOException {
        FTPUtil ftpUtil=new FTPUtil(ftpIp,21,ftpUser,ftpPass);
        logger.info("开始连接ftp服务器");
        boolean result=ftpUtil.uploadFile("img",fileList);

        logger.info("开始连接ftp服务器，结束上传，上传结果{}",result);
        return result;
    }

    /**
     * 上传FTP服务器
     * @param remotePath 远程FTP 服务器路径
     * @param fileList 文件
     * @return boolean
     */
    private boolean uploadFile(String remotePath,List<File> fileList) throws IOException {
        boolean uploaded=true;
        FileInputStream fis=null;
        //连接FTP服务器
        if(connectServer(this.ip,this.port,this.user,this.pwd)){
            try {
                ftpClient.changeWorkingDirectory(remotePath);//路径
                ftpClient.setBufferSize(1024);//设置缓冲区
                ftpClient.setControlEncoding("UTF-8");//设置文件格式
                ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);//把文件设置成为一个二进制类型（防止乱码）
                ftpClient.enterLocalPassiveMode();//打开本地的被动模式
                for(File fileItem :fileList){
                    fis =new FileInputStream(fileItem);
                    ftpClient.storeFile(fileItem.getName(),fis);
                }
            } catch (IOException e) {
                logger.error("上传文件异常",e);
                uploaded=false;
                e.printStackTrace();
            }finally{
                //释放资源
                fis.close();
                ftpClient.disconnect();
            }
        }
        return uploaded;
    }

    /**
     * 连接FTP服务器
     * @param ip 服务器ip
     * @param prot 服务器端口
     * @param user 用户名
     * @param pwd 密码
     * @return boolean
     */
    private boolean connectServer(String ip, int prot,String user,String pwd){
        boolean isSucces=false;
        ftpClient=new FTPClient();
        try {
            ftpClient.connect(ip);
            isSucces=ftpClient.login(user,pwd);
        } catch (IOException e) {
            logger.error("连接FTP服务器异常",e);
        }
        return isSucces;
    }

    private String ip;
    private int port;
    private String user;
    private String pwd;

    private FTPClient ftpClient;


    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public FTPClient getFtpClient() {
        return ftpClient;
    }

    public void setFtpClient(FTPClient ftpClient) {
        this.ftpClient = ftpClient;
    }

}
