package com.mmall.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

/**
 * 时间类工具装换
 * Created by 17936 on 2017/8/19.
 */
public class DateTimeUtil {

    public static final String STANDARD_FORMAT="yyy-MM-dd HH:mm:ss";

    /**
     * Str->>Date
     * @param dateTimeStr
     * @param formatStr
     * @return
     */
    public static Date strToDate(String dateTimeStr, String formatStr){
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(formatStr);
        DateTime dateTime =dateTimeFormat.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }
    public static Date strToDate(String dateTimeStr){
        DateTimeFormatter dateTimeFormat = DateTimeFormat.forPattern(STANDARD_FORMAT);
        DateTime dateTime =dateTimeFormat.parseDateTime(dateTimeStr);
        return dateTime.toDate();
    }

    /**
     * Date->>Str
     * @param date
     * @param formatStr
     * @return
     */
    public static String dateToStr(Date date, String formatStr){
        if(date==null){
            return StringUtils.EMPTY;
        }
        DateTime dateTime =new DateTime();
        return dateTime.toString(formatStr);
    }
    public static String dateToStr(Date date){
        if(date==null){
            return StringUtils.EMPTY;
        }
        DateTime dateTime =new DateTime();
        return dateTime.toString(STANDARD_FORMAT);
    }

    public static void main(String[] args) {
        System.out.println(DateTimeUtil.dateToStr(new Date(),"yyy-MM-dd HH:mm:ss"));
        System.out.println(DateTimeUtil.strToDate("2017-03-02 11:11:11","yyy-MM-dd HH:mm:ss"));
    }
}
