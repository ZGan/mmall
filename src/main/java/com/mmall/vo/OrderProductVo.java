package com.mmall.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * 选中购物车中商品信息
 * Created by 17936 on 2017/11/11.
 */
public class OrderProductVo {
    private List<OrderItemVo> orderitemVoList;
    private BigDecimal productTotalPrice;
    private String imageHost;

    public List<OrderItemVo> getOrderitemVoList() {
        return orderitemVoList;
    }

    public void setOrderitemVoList(List<OrderItemVo> orderitemVoList) {
        this.orderitemVoList = orderitemVoList;
    }

    public BigDecimal getProductTotalPrice() {
        return productTotalPrice;
    }

    public void setProductTotalPrice(BigDecimal productTotalPrice) {
        this.productTotalPrice = productTotalPrice;
    }

    public String getImageHost() {
        return imageHost;
    }

    public void setImageHost(String imageHost) {
        this.imageHost = imageHost;
    }
}
