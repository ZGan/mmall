package com.mmall.vo;

/**
 * socket 实时网签vo
 * Created by pc on 2017/11/2.
 */
public class XMLrealTimeVo {
    public String CONTRACT_NO;//合同编号
    public String LOAN_CATE;//贷款类别
    public String CUSTOMER_TYPE;//借款人类别
    public String CUSTOMER_NAME;//借款人名称
    public String CERTIFICATE_TYPE;//借款人证件类型
    public String CERTIFICATE_NO;//借款人证件号码
    public String CONTRACT_AMOUNT;//合同金额
    public String INT_RATE;//利率
    public String CONTRACT_SIGN_DATE;//合同签订日期

    public String getCONTRACT_NO() {
        return CONTRACT_NO;
    }

    public void setCONTRACT_NO(String CONTRACT_NO) {
        this.CONTRACT_NO = CONTRACT_NO;
    }

    public String getLOAN_CATE() {
        return LOAN_CATE;
    }

    public void setLOAN_CATE(String LOAN_CATE) {
        this.LOAN_CATE = LOAN_CATE;
    }

    public String getCUSTOMER_TYPE() {
        return CUSTOMER_TYPE;
    }

    public void setCUSTOMER_TYPE(String CUSTOMER_TYPE) {
        this.CUSTOMER_TYPE = CUSTOMER_TYPE;
    }

    public String getCUSTOMER_NAME() {
        return CUSTOMER_NAME;
    }

    public void setCUSTOMER_NAME(String CUSTOMER_NAME) {
        this.CUSTOMER_NAME = CUSTOMER_NAME;
    }

    public String getCERTIFICATE_TYPE() {
        return CERTIFICATE_TYPE;
    }

    public void setCERTIFICATE_TYPE(String CERTIFICATE_TYPE) {
        this.CERTIFICATE_TYPE = CERTIFICATE_TYPE;
    }

    public String getCERTIFICATE_NO() {
        return CERTIFICATE_NO;
    }

    public void setCERTIFICATE_NO(String CERTIFICATE_NO) {
        this.CERTIFICATE_NO = CERTIFICATE_NO;
    }

    public String getCONTRACT_AMOUNT() {
        return CONTRACT_AMOUNT;
    }

    public void setCONTRACT_AMOUNT(String CONTRACT_AMOUNT) {
        this.CONTRACT_AMOUNT = CONTRACT_AMOUNT;
    }

    public String getINT_RATE() {
        return INT_RATE;
    }

    public void setINT_RATE(String INT_RATE) {
        this.INT_RATE = INT_RATE;
    }

    public String getCONTRACT_SIGN_DATE() {
        return CONTRACT_SIGN_DATE;
    }

    public void setCONTRACT_SIGN_DATE(String CONTRACT_SIGN_DATE) {
        this.CONTRACT_SIGN_DATE = CONTRACT_SIGN_DATE;
    }
}
