package com.mmall.controller.backend;

import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.ICategoryService;
import com.mmall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * 商品品类
 * Created by 17936 on 2017/8/18.
 */
@Controller
@RequestMapping("/manage/category")
public class CategoryManageController {

    @Autowired
    private IUserService iUserService;

    @Autowired
    private ICategoryService iCategoryService;

    /**
     * 增加商品品类
     * @param session session
     * @param categoryName 品类名称
     * @param parentId 节点id（如果前端不传，默认值为0）
     * @return
     */
    @RequestMapping("add_category.do")
    @ResponseBody
    public ServerResponse<String> addCatgory(HttpSession session, String categoryName, @RequestParam(value="parentId" ,defaultValue = "0") Integer parentId){
        User user =(User) session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");//返回10要求强制登录
        }
        //校验是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //是管理员执行增加方法
            return iCategoryService.addCategory(categoryName, parentId);
        }else{
            return ServerResponse.createByErrorMessage("不是管理员没有操作权限，需要管理员权限");
        }
    }

    /**
     * 修改商品名称
     * @param session session
     * @param categoryName 商品品类名称
     * @param categoryId 商品品类id
     * @return
     */
    @RequestMapping("set_category_name.do")
    @ResponseBody
    public ServerResponse<String> setCategoryName(HttpSession session,String categoryName,Integer categoryId){
        User user =(User) session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");//返回10要求强制登录
        }
        //校验是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //是管理员，执行修改方法
            return  iCategoryService.upcateCategoryName(categoryName, categoryId);
        }else{
            return ServerResponse.createByErrorMessage("不是管理员没有操作权限，需要管理员权限");
        }
    }

    /**
     * 查询子节点的信息，不递归，保持平级
     * @param session session
     * @param categoryId 子节点id ，不传默认为0
     * @return
     */
    @RequestMapping("get_category.do")
    @ResponseBody
    public ServerResponse getChildrenParallelCategory(HttpSession session,@RequestParam(value = "categoryId",defaultValue = "0") Integer categoryId){
        User user =(User) session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");//返回10要求强制登录
        }
        //校验是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //是管理员，执行查询方法
            return  iCategoryService.getChildrenParallelCategory(categoryId);
        }else{
            return ServerResponse.createByErrorMessage("不是管理员没有操作权限，需要管理员权限");
        }
    }

    /**
     * 查询当前节点的id和递归节点的id（0->10000->100000）
     * @param session session
     * @param categoryId 子节点id ，不传默认为0
     * @return
     */
    @RequestMapping("get_deep_category.do")
    @ResponseBody
    public ServerResponse getCategoryAndDeepChildrenCategory(HttpSession session,@RequestParam(value = "categoryId",defaultValue = "0") Integer categoryId){
        User user =(User) session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录");//返回10要求强制登录
        }
        //校验是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //是管理员，执行查询方法
            return  iCategoryService.selectCategoryAndChildrenById(categoryId);
        }else{
            return ServerResponse.createByErrorMessage("不是管理员没有操作权限，需要管理员权限");
        }
    }

}
