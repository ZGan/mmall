package com.mmall.controller.backend;

import com.github.pagehelper.PageInfo;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;
import com.mmall.service.IOrderService;
import com.mmall.service.IUserService;
import com.mmall.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * 后台订单模块
 * Created by 17936 on 2017/11/11.
 */
@Controller
@RequestMapping("/manage/order")
public class OrderManageController {
    @Autowired
    private IOrderService iOrderService;

    @Autowired
    private IUserService iUserService;
    /**
     * 订单list页
     * @param session session
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return
     */
    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse orderList(HttpSession session , @RequestParam(value="pageNum",defaultValue = "1")int pageNum,
                                    @RequestParam(value = "pageSize",defaultValue = "10")  int pageSize){
        User user =(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录管理员");
        }
        //判断是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //执行逻辑
            return  iOrderService.manageList(pageNum,pageSize);
        }else{
            return ServerResponse.createByErrorMessage("无操作权限");
        }
    }

    /**
     * 订单详情
     * @param session session
     * @param orderNo 订单号
     * @return ServerResponse
     */
    @RequestMapping("detail.do")
    @ResponseBody
    public ServerResponse<OrderVo> orderDetail(HttpSession session, Long orderNo){
        User user =(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录管理员");
        }
        //判断是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //执行逻辑
            return iOrderService.manageDetail(orderNo);
        }else{
            return ServerResponse.createByErrorMessage("无操作权限");
        }
    }

    /**
     * 根据订单号查询订单
     * @param session session
     * @param orderNo 订单号
     * @return OrderVo
     */
    @RequestMapping("search.do")
    @ResponseBody
    public ServerResponse<PageInfo> orderSearch(HttpSession session, Long orderNo, @RequestParam(value="pageNum",defaultValue = "1")int pageNum,
                                                @RequestParam(value = "pageSize",defaultValue = "10")  int pageSize){
        User user =(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录管理员");
        }
        //判断是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //执行逻辑
            return iOrderService.manageSearch(orderNo,pageNum,pageSize);
        }else{
            return ServerResponse.createByErrorMessage("无操作权限");
        }
    }

    /**
     * 发货功能
     * @param session session
     * @param orderNo orderNo
     * @return String
     */
    @RequestMapping("send_goods.do")
    @ResponseBody
    public ServerResponse<String> OrderSendGoods(HttpSession session, Long orderNo){
        User user =(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return  ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),"用户未登录，请登录管理员");
        }
        //判断是否为管理员
        if(iUserService.chekAdminRole(user).isSuccess()){
            //执行逻辑
            return iOrderService.orderSendGoods(orderNo);
        }else{
            return ServerResponse.createByErrorMessage("无操作权限");
        }
    }

}
