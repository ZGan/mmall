package com.mmall.controller.portal;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.service.IProductService;
import com.mmall.vo.ProductDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 前台商品功能
 * Created by 17936 on 2017/9/2.
 */
@Controller
@RequestMapping("/product/")
public class ProductController {

    @Autowired
    private IProductService iProductService;

    /**
     * 前台商品详情
     * @param productId 商品ID
     * @return ProductListVo
     */
    @RequestMapping("detail.do")
    @ResponseBody
    public ServerResponse<ProductDetailVo> detail(Integer productId){
       return iProductService.getProductDetail(productId);
    }

    /**
     * 前台分页根据关键字和分类节点id查询并实现动态排序
     * @param keyword 名字
     * @param categoryId 节点id
     * @param pageNum 第几页
     * @param pageSize 每页多少条
     * @param orderBy 排序
     * @return PageInfo
     */
    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse<PageInfo> list(@RequestParam(value = "keyword" ,required = false) String keyword,
                                         @RequestParam(value = "categoryId" ,required = false) Integer categoryId,
                                         @RequestParam(value ="pageNum" ,defaultValue = "1") int pageNum,
                                         @RequestParam(value ="pageSize" ,defaultValue = "10") int pageSize,
                                         @RequestParam(value ="orderBy" ,defaultValue = "") String orderBy){

        return  iProductService.getProductKeywordCategory(keyword,categoryId,pageNum,pageSize,orderBy) ;
    }
}
