package com.mmall.controller.portal;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.demo.trade.config.Configs;
import com.google.common.collect.Maps;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;

import com.mmall.pojo.User;
import com.mmall.service.IOrderService;
import com.mysql.jdbc.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Iterator;
import java.util.Map;

/**
 * 订单模块
 * Created by Administrator on 2017/9/27.
 */
@Controller
@RequestMapping("/order/")
public class OrderController {

    private  static  final Logger log= LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private IOrderService iOrderService;

    /**
     * 支付并生成二维码
     * @param session  session
     * @param orderNo 订单号
     * @param request  request
     * @return
     */
    @RequestMapping("pay.do")
    @ResponseBody
    public ServerResponse pay(HttpSession session, Long orderNo, HttpServletRequest request){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        String path=request.getSession().getServletContext().getRealPath("upload");
        return iOrderService.pay(orderNo,user.getId(),path);
    }

    /**
     * 支付宝回调(支付宝的回调函数都放到了request里)
     * @param request request
     * @return Object
     */
    @RequestMapping("alipay_callback.do")
    @ResponseBody
    public Object alipayCallback(HttpServletRequest request){
        Map<String ,String> parmas= Maps.newHashMap();
        Map requesParams= request.getParameterMap();
        //迭代器遍历Map
        for (Iterator iter =requesParams.keySet().iterator();iter.hasNext();){
            String name=(String)iter.next();
            String[] values=(String[])requesParams.get(name);
            String valueStr="";
            for(int i=0;i<values.length;i++){
                valueStr=(i==values.length-1)?valueStr+values[i]:valueStr+values[i]+",";
            }
            parmas.put(name,valueStr);
        }
        log.info("支付宝回调：异步回调验签sign:{},交易状态trade_status:{},参数：{}",parmas.get("sign"),parmas.get("trade_status"),parmas.toString());

        //非常重要，非常重要，非常重要，验证回调函数的正确性，是不是支付宝发的，而且避免重复通知
        parmas.remove("sign_type");
        try {
            boolean alipayRSACheckecdV2= AlipaySignature.rsaCheckV2(parmas, Configs.getAlipayPublicKey(),"utf-8",Configs.getSignType());
            if(!alipayRSACheckecdV2){
                return  ServerResponse.createByErrorMessage("非法请求，验证不通过，在恶意请求我就报警找网警了");
            }
        } catch (AlipayApiException e) {
            log.info("支付宝回调异常");
        }
        // TODO: 2017/9/27  验证各种数据
        ServerResponse serverResponse= iOrderService.aliCallback(parmas);
        if(serverResponse.isSuccess()){
            return Const.AlipayCallback.RESPONSE_SUCCESS;//支付成功返回“success”给支付宝
        }
        return Const.AlipayCallback.RESPONSE_FAILED;//支付失败返回“failed”给支付宝
    }

    /**
     * 查询订单是否支付成功
     * @param session session
     * @param orderNo 订单编号
     * @return Boolean
     */
    @RequestMapping("query_order_pay_status.do")
    @ResponseBody
    public ServerResponse<Boolean> queryOrederPayStatus(HttpSession session, Long orderNo){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        ServerResponse serverResponse=iOrderService.queryOderPayStatus(user.getId(),orderNo);
        if(serverResponse.isSuccess()){
            return ServerResponse.createBySuccess(true);
        }
        return ServerResponse.createBySuccess(false);
    }

    /**
     * 创建订单
     * @param session  session
     * @param shippingId 收货地址编号
     * @return ServerResponse
     */
    @RequestMapping("create.do")
    @ResponseBody
    public ServerResponse create(HttpSession session,Integer shippingId){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        return iOrderService.createOrder(user.getId(),shippingId);
    }

    /**
     * 取消订单
     * @param session session
     * @param orderNo 订单号
     * @return ServerResponse
     */
    @RequestMapping("cancel.do")
    @ResponseBody
    public ServerResponse cancel(HttpSession session,Long orderNo){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        return iOrderService.cancel(user.getId(),orderNo);
    }

    /**
     * 获取选中购物车中商品信息
     * @param session session
     * @return ServerResponse
     */
    @RequestMapping("get_order_cart_product.do")
    @ResponseBody
    public ServerResponse getOrderCartProduct(HttpSession session){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        return iOrderService.getOrderCartProduct(user.getId());
    }

    /**
     * 订单详情
     * @param session session
     * @param orderNo orderNo
     * @return ServerResponse
     */
    @RequestMapping("detail.do")
    @ResponseBody
    public ServerResponse detail(HttpSession session,long orderNo){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        return iOrderService.getOrderDetail(user.getId(),orderNo);
    }

    /**
     * 订单list
     * @param session session
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return ServerResponse
     */
    @RequestMapping("list.do")
    @ResponseBody
    public ServerResponse list(HttpSession session, @RequestParam(value = "pageNum",defaultValue = "1")int pageNum, @RequestParam(value = "pageSize",defaultValue = "10")int pageSize){
        User user=(User)session.getAttribute(Const.CURRENT_USER);
        if(user==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getDesc());//返回10，需要强制登录
        }
        return iOrderService.getOrderList(user.getId(),pageNum,pageSize);
    }


}
