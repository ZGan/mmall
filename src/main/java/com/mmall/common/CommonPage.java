package com.mmall.common;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class CommonPage implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 7941149355570542666L;
    public Integer pageSize; //每页条数
    public Integer curr;//当前页。

    public Integer pages;//总页数

    public String tableFiled;//优化的主键
    public String tableName;//优化的查询分页表名称
    public String tableAglisName;// 表别名

    public String html;

    public Map<String,Object> paramMap;

    public String getHtml() {
        return html;
    }

    public void setHtml(String html) {
        this.html = html;
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }

    public String getTableAglisName() {
        return tableAglisName;
    }

    public void setTableAglisName(String tableAglisName) {
        this.tableAglisName = tableAglisName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableFiled() {
        return tableFiled;
    }

    public void setTableFiled(String tableFiled) {
        this.tableFiled = tableFiled;
    }


    public Object pageData;
    public static Integer DEFAULT_PAGESIZE = 10;

    public CommonPage() {

    }


    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer totalRecord) {
        this.pages = totalRecord;
    }

//	public Integer getTotalPage()       {
//		return totalPage;
//	}
//
//	public void setTotalPage(Integer totalPage) {
//		this.totalPage = totalPage;
//	}

    public Object getPageData() {
        return pageData;
    }

    public void setPageData(Object pageData) {
        this.pageData = pageData;
    }


    public Integer getPageSize() {
        if (pageSize == null) return DEFAULT_PAGESIZE;
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCurr() {
        if (curr == null) return 0;
        return curr;
    }

    public void setCurr(Integer curr) {
        this.curr = curr;
    }

    //	public String getOrder() {
//		return order;
//	}
//
//	public void setOrder(String order) {
//		this.order = order;
//	}

	/**
	 *
	 * @param
	 * @return
	 */
	public Map<String, Object> pageToMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("page", this);
         if(paramMap!=null){
             map.putAll(paramMap);
         }
		return map;
	}

    public JSONObject toSHowPage() {
        JSONObject getObj = new JSONObject();
        getObj.put("pages", pages);//实际的行数
        getObj.put("iTotalDisplayRecords", pages);//显示的行数,这个要和上面写的一样
        if (pageData != null) {
            getObj.put("aaData", pageData);//要以JSON格式返回
        } else {
            getObj.put("aaData", new ArrayList());//要以JSON格式返回
        }


        return getObj;
    }

    /**
     * @param columns_name 要在页面表格中显示的字段
     * @param columns      页面表格中显示的字段对应数据中的字段
     * @param url          页面数据url请求地址
     * @return
     */
    public static Map<String, Object> setColumnToPage(String[] columns_name, Object[] columns, String url, String titlename) {
        Map<String, Object> map_columns = new LinkedHashMap<String, Object>();
        Map<String, Object> map_url = new HashMap<String, Object>();
        Map<String, Object> map_title = new HashMap<String, Object>();
        Map<String, Object> map = new HashMap<String, Object>();
        map_url.put("url", url);
        map_title.put("title", titlename);
        if(columns_name!=null && columns!=null){
            for (int i = 0; i < columns.length; i++) {
                map_columns.put(columns_name[i], columns[i]);
                map.put("columns", map_columns);
            }
        }

        map.put("url", map_url);
        map.put("title", map_title);
        return map;
    }

    /**
     * @param id   树节点数据的id字段
     * @param text 要显示在前端的字段，和传入后台的数据字段名相同
     * @param pid  树节点数据的父id字段
     * @param url  获取树节点数据的url请求地址
     * @return
     */
    public static Map<String, Object> setTreeToPage(String id, String text, String pid, String url) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("id", id);
        map.put("text", text);
        map.put("pid", pid);
        map.put("url", url);
        return map;
    }





}
