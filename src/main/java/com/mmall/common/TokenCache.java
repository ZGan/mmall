package com.mmall.common;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;

/**
 * 缓冲存储器（令牌）
 * Created by 17936 on 2017/8/13.
 */
public class TokenCache {

    private static Logger logger= LoggerFactory.getLogger(TokenCache.class);

    public static  final  String TOKEN_PREFIX="token_";

    //设置参数的初始化容量1000最大容量10000,有效期为12小时（超过最大容量就使用LRU算法清除缓存）
    private static LoadingCache<String ,String> localCache= CacheBuilder.newBuilder().initialCapacity(1000).maximumSize(10000).expireAfterAccess(12, TimeUnit.HOURS)
            .build(new CacheLoader<String, String>() {
                //默认数据加载的实现，当调用get取值的时候，如果key没有对应的值的，就调用这个方法进行加载、
                @Override
                public String load(String key) throws Exception {
                    return "null";
                }
            });

    public static void setKey(String key,String value){
        localCache.put(key, value);
    }

    public static String getKey(String key){
        String value="null";
        try{
            value=localCache.get(key);
            if("null".equals(value)){
                return null;
            }
            return value;
        }catch (Exception e){
            logger.error("localCache get error",e);
        }
        return  null;
    }


}
