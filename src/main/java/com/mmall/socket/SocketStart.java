package com.mmall.socket;

import com.mmall.util.PropertiesUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * 连接socket
 * Created by pc on 2017/11/2.
 */
public class SocketStart {
    public static String sendXML(String msg){
        String responseMessage =null;
        try{
            Socket socket = new Socket(PropertiesUtil.getProperty("socket.put.ip"),Integer.valueOf(PropertiesUtil.getProperty("socket.put.popt")));
            socket.setSoTimeout(30000);
            System.out.println("连接上啦");
            System.out.println("正在发送");
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),"UTF-8"));
            pw.println(msg);
            System.out.println("要发送的数据:"+msg);
            pw.flush();
            System.out.println("发送完成等待回应");
            BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream(),"UTF-8"));
            responseMessage = br.readLine();
            pw.close();
            br.close();
            socket.close();
            System.out.println(responseMessage);
            System.out.println("程序正常结束");
        }catch(Exception e){
            responseMessage="err";
            e.printStackTrace();
            System.out.println("连接被中断了");
        }
        return responseMessage;
    }
}
