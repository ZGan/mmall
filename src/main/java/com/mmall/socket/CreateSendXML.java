package com.mmall.socket;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.mmall.util.DateTimeUtil;
import com.mmall.util.PropertiesUtil;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import com.mmall.vo.XMLrealTimeVo;
import org.junit.Test;

/**
 * 生成报文
 * Created by pc on 2017/11/2.
 */
public class CreateSendXML {

    private static String headStrings=PropertiesUtil.getProperty("socket.put.headString");

    private static String filePaths=PropertiesUtil.getProperty("socket.put.path");

    /**
     * 生成并上传socket
     * @param vo vo
     * @return 响应信息
     */
    public static String xmlNet(XMLrealTimeVo vo) {
        String result=null;
        Date time = new Date();
        String now1 = new SimpleDateFormat("yyyyMMdd-HHmmssSSS").format(time);// 某时间点;
        String aString =randomNUM(1);
        try {
            String fullSubmitFile = localPath(now1);
            Document xmldocument = DocumentHelper.createDocument();
            // 创建根节点
            Element transactionRoot = xmldocument.addElement("transaction");
            // 创建头
            Element headElement = transactionRoot.addElement("header");
            // 创建msg
            Element msgElement = headElement.addElement("msg");
            // 创建内容
            Element serEle = msgElement.addElement("SERVICE_CODE");// 1服务代码
            serEle.setText("SVR_PTLN");
            Element codeEle = msgElement.addElement("TRAN_CODE");// 2交易码
            codeEle.setText("PTLN001");

            Element modeEle = msgElement.addElement("TRAN_MODE");// 4交易模式
            modeEle.setText("ONLINE");
            Element branchEle = msgElement.addElement("BRANCH_ID");// 5组织机构代码
            branchEle.setText(PropertiesUtil.getProperty("socket.put.headString"));// 组织机构代码
            Element dateEle = msgElement.addElement("TRAN_DATE");// 6交易日期YYYYMMDD
            String dates = DateTimeUtil.dateToStr(new Date(), "yyyyMMdd");
            dateEle.setText(dates);
            Element stampEle = msgElement.addElement("TRAN_TIMESTAMP");// 7交易时间HHMMSSNNN
            stampEle.setText(now1);

            Element langEle = msgElement.addElement("USER_LANG");// 10用户语言
            langEle.setText("CHINESE");
            Element seqEle = msgElement.addElement("SEQ_NO");// 11渠道流水号
            seqEle.setText(DateTimeUtil.dateToStr(new Date(), "yyyyMMddHHssSSS")+aString);// 小贷公司保证自身流水号不重复

            Element moduleEle = msgElement.addElement("MODULE_ID");// 14模块标识 CL
            moduleEle.setText("CL");
            Element mesEle = msgElement.addElement("MESSAGE_TYPE");// 15报文类型
            mesEle.setText("1200");
            Element mesCodeEle = msgElement.addElement("MESSAGE_CODE");// 16报文代码
            mesCodeEle.setText("0001");
            // 取值参加相关服务接口描述
            Element filePathEle = msgElement.addElement("FILE_PATH");// 17文件路径
            // 文件名规则：组织机构代码-YYYYMMDD-数据类型-XX.xm
            Element bodyElement = transactionRoot.addElement("body");// body
            Element gettxElement = bodyElement.addElement("GetTx");

            Element CONTRACT_NO = gettxElement.addElement("CONTRACT_NO");
            CONTRACT_NO.setText(vo.getCONTRACT_NO());//合同编号

            Element LOAN_CATE = gettxElement.addElement("LOAN_CATE");
            LOAN_CATE.setText("530001");//贷款类别

            Element CUSTOMER_TYPE = gettxElement.addElement("CUSTOMER_TYPE");
            CUSTOMER_TYPE.setText("480001");//借款人类别

            Element CUSTOMER_NAME = gettxElement.addElement("CUSTOMER_NAME");
            CUSTOMER_NAME.setText(vo.getCUSTOMER_NAME());//借款人名称

            Element CERTIFICATE_TYPE = gettxElement.addElement("CERTIFICATE_TYPE");
            CERTIFICATE_TYPE.setText("150001");//借款人证件类型

            Element CERTIFICATE_NO = gettxElement.addElement("CERTIFICATE_NO");
            CERTIFICATE_NO.setText(vo.getCERTIFICATE_NO());//借款人证件号码

            Element CONTRACT_AMOUNT = gettxElement.addElement("CONTRACT_AMOUNT");
            CONTRACT_AMOUNT.setText(vo.getCONTRACT_AMOUNT());//合同金额

            Element INT_RATE = gettxElement.addElement("INT_RATE");
            INT_RATE.setText(vo.getINT_RATE());//月利率

            Element CONTRACT_SIGN_DATE = gettxElement.addElement("CONTRACT_SIGN_DATE");
            CONTRACT_SIGN_DATE.setText(vo.getCONTRACT_SIGN_DATE());//合同签订日期

            // 创建xml文件
            File destinationFile = new File(fullSubmitFile);
            destinationFile.createNewFile();

            OutputFormat format = OutputFormat.createPrettyPrint();
            //去除换行空格
            format.setNewLineAfterDeclaration(false);
            format.setNewlines(false);
            format.setIndent(false);

            XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(destinationFile), "utf-8"), format);
            int number=xmldocument.asXML().getBytes("UTF-8").length;
            System.out.println("值："+xmldocument);
            System.out.println("长度:"+number);
            NumberFormat formatter = NumberFormat.getNumberInstance();
            formatter.setMinimumIntegerDigits(8);
            formatter.setGroupingUsed(false);
            String s = formatter.format(number);
            writer.write(s);
            writer.write(xmldocument);//数据写入生成的xml中
            writer.close();
            System.out.println("生成实时网签报文已完成");
            //socket上报
            result=SocketStart.sendXML(s+xmldocument.asXML());
        }catch (Exception e){
            System.out.println("生成实时网签报文上报失败");
            e.printStackTrace();
        }
        return result;
    }
    //文件路径
    private static String localPath(String  filesubmittime) {
        String fullSubmitFile = "";
        try {
            File file = new File(filePaths);
            file.mkdir();
            String headString = headStrings;// 组织机构代码
            String fullSubmitFileName = headString + "-" + filesubmittime+ ".xml";
            // 文件路径
            fullSubmitFile = file + "\\" + fullSubmitFileName;
            System.out.println("fullSubmitFile:" + fullSubmitFile);
        } catch (Exception e) {
            System.err.println("生成实时网签报文失败");
            e.printStackTrace();
        }
        return fullSubmitFile;
    }

    /**
     * 生成随机数
     * @param NUM 要生成随机数的个数
     * @return 随机数
     */
    private static String randomNUM(int NUM){
        int c[] = new int[NUM];
        String aString = "";
        for (int aas = 0; aas < c.length; aas++){
            c[aas] = (int) (10 * (Math.random()));
            aString += c[aas];
        }
        return aString;
    }

    @Test
    public void test3(){
        String aString =randomNUM(123);
        System.out.printf(aString);
    }

    @Test
    public void test16(){
        XMLrealTimeVo vo =new XMLrealTimeVo();
        vo.setCONTRACT_NO("123123123h");
        vo.setLOAN_CATE("530001");
        vo.setCUSTOMER_TYPE("480001");
        vo.setCUSTOMER_NAME("123");
        vo.setCERTIFICATE_TYPE("150001");
        vo.setCERTIFICATE_NO("43042219838381818");
        vo.setCONTRACT_AMOUNT("12312313");
        vo.setINT_RATE("0.1");
        vo.setCONTRACT_SIGN_DATE("20170920");
        CreateSendXML.xmlNet(vo);
    }
}
