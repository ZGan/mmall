package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.vo.CartVo;

/**
 * 购物车模块
 * Created by 17936 on 2017/9/3.
 */
public interface ICartService {

    ServerResponse<CartVo> add(Integer userId,Integer count,Integer productId);//增加商品到购物车

    ServerResponse<CartVo>  update(Integer userId,Integer count,Integer productId);//更新购物车商品数量

    ServerResponse<CartVo> deleteProduct(Integer userId,String productIds);//批量删除购物车里面的商品

    ServerResponse<CartVo> list(Integer userId);//查询全部购物车的产品

    ServerResponse<CartVo> selectOrUnSelect(Integer userId,Integer checked,Integer productId);//全选或者全反选

    ServerResponse<Integer> getCartProductCount(Integer userId);//查询购物车产品数量

}
