package com.mmall.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 文件处理
 * Created by 17936 on 2017/8/22.
 */
public interface IFileService {

    String upload(MultipartFile file, String path);//文件上传

}
