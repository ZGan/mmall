package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;

import java.util.Map;

/**
 * 订单模块（支付模块）
 * Created by Administrator on 2017/9/27.
 */
public interface IOrderService {

    ServerResponse pay(Long orderNo, Integer userId, String path);//支付并生成二维码

    ServerResponse aliCallback(Map<String,String> params);//回调本地校验

    ServerResponse<Boolean> queryOderPayStatus(Integer userId,Long orderNo);//查询订单是否支付成功

    ServerResponse createOrder(Integer userId,Integer shippingId);//创建新订单

    ServerResponse<String> cancel(Integer userId,Long orderNo);//取消订单

    ServerResponse getOrderCartProduct(Integer userId);//获取选中购物车中商品信息

    ServerResponse getOrderDetail(Integer userId,Long orderNo);//订单详情

    ServerResponse<PageInfo> getOrderList(Integer userId, int pageNum, int pageSize);//订单list
    //backend

    ServerResponse<PageInfo> manageList(int pageNum,int pageSize);// 后台订单列表

    ServerResponse manageDetail(Long orderNo);// 后台订单详情

    ServerResponse<PageInfo> manageSearch(Long orderNo,int pageNum,int pageSize);//根据订单号后台搜索

    ServerResponse<String> orderSendGoods(Long orderNo);//订单发货


}
