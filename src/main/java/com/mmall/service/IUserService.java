package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.User;

/**
 * 用户模块
 * Created by 17936 on 2017/8/6.
 */
public interface IUserService {

    ServerResponse<User> login(String username, String password);//登录

    ServerResponse<String> register(User user);//注册、校验

    ServerResponse<String> checkValid(String str,String type);//校验

    ServerResponse<String> selectQuestion(String username);//根据用户名找到密码问题

    ServerResponse<String> checkAnswer(String username,String question, String answer);//根据问题答案校验用户（key放入本地缓存）

    ServerResponse<String> forgetResetPassword(String username,String passwordNew,String forgetToken);//忘记密码中重置密码（未登录）

    ServerResponse<String> resetPassword(String passwordOld,String passwordNew,User user);//登录状态下重置密码

    ServerResponse<User> upateInformation(User user);//更新个人用户信息

    ServerResponse<User> getInformation(Integer userId);//获取用户详情并把得到的密码set空

    ServerResponse chekAdminRole(User user);//校验当前登录的用户是否为管理员

}
