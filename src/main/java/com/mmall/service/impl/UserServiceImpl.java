package com.mmall.service.impl;

import com.mmall.common.Const;
import com.mmall.common.ServerResponse;
import com.mmall.common.TokenCache;
import com.mmall.dao.UserMapper;
import com.mmall.pojo.User;
import com.mmall.service.IUserService;
import com.mmall.util.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * 用户业务逻辑层
 * Created by 17936 on 2017/8/6.
 */
@Service("iUserService")
public class UserServiceImpl implements   IUserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return
     */
    @Override
    public ServerResponse<User> login(String username, String password) {
        int resultCount = userMapper.checkUsername(username);
        if(resultCount == 0 ){
            return ServerResponse.createByErrorMessage("用户名不存在");
        }

        String md5Password = MD5Util.MD5EncodeUtf8(password);
        User user  = userMapper.selectLogin(username,md5Password);
        if(user == null){
            return ServerResponse.createByErrorMessage("密码错误");
        }
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess("登录成功",user);
    }

    /**
     * 注册、校验(普通用户)
     * @param user
     * @return
     */
    public ServerResponse<String> register(User user){
        ServerResponse validResponse = this.checkValid(user.getUsername(),Const.USERNAME);
        if(!validResponse.isSuccess()){
            return validResponse;
        }
        validResponse = this.checkValid(user.getEmail(),Const.EMAIL);
        if(!validResponse.isSuccess()){
            return validResponse;
        }
        user.setRole(Const.Role.ROLE_CUSTOMER);
        String password=user.getPassword();
        //MD5加密
        user.setPassword(MD5Util.MD5EncodeUtf8(password));
        int resultCount = userMapper.insert(user);
        if(resultCount == 0){
            return ServerResponse.createByErrorMessage("注册失败");
        }
        return ServerResponse.createBySuccessMessage("注册成功");
    }

    /**
     * 校验
     * @param str 传的值
     * @param type 校验的值（username,Email）
     * @return
     */
    public ServerResponse<String> checkValid(String str,String type){
        if(StringUtils.isNotBlank(type)){
            //开始校验(Email)
            if(Const.EMAIL.equals(type)){
                int resultCount=userMapper.checkEmail(str);
                if(resultCount>0){
                    return  ServerResponse.createByErrorMessage("Email已经存在");
                }
            }
            //开始校验(用户名)
            if(Const.USERNAME.equals(type)){
                int resultCount=userMapper.checkUsername(str);
                if(resultCount>0){
                    return  ServerResponse.createByErrorMessage("用户名已经存在");
                }
            }
        }else{
            return ServerResponse.createByErrorMessage("校验失败");
        }
        return  ServerResponse.createBySuccessMessage("校验成功");
    }

    /**
     * 忘记密码，根据用户名找到提示问题
     * @param username 用户名
     * @return
     */
    public ServerResponse<String> selectQuestion(String username){
        ServerResponse validResponse=this.checkValid(username,Const.USERNAME);
        if(validResponse.isSuccess()){
            //用户名不存在
            return ServerResponse.createByErrorMessage("用户名不存在");
        }
        String question=userMapper.selectQuestionByUsername(username);
        if(StringUtils.isNotBlank(question)){
            return ServerResponse.createBySuccess(question);
        }
        return ServerResponse.createByErrorMessage("找回密码的问题是空的");
    }

    /**
     * 根据用户问题答案校验用户（key放入本地缓存）
     * @param username 用户名
     * @param question 问题
     * @param answer 问题答案
     * @return
     */
    public ServerResponse<String> checkAnswer(String username,String question, String answer){
        int resultCount=userMapper.checkAnswer(username, question, answer);
        if(resultCount>0){
            //说明问题及问题答案是这个用户，并且是正确的
            String forgetToken= UUID.randomUUID().toString();//生成随机唯一的字符串
            TokenCache.setKey(TokenCache.TOKEN_PREFIX+username,forgetToken);//放入本地缓存令牌中
            return ServerResponse.createBySuccess(forgetToken);
        }
        return ServerResponse.createByErrorMessage("问题的答案错误");
    }

    /**
     * 忘记密码中重置密码（未登录）
     * @param username 用户名
     * @param passwordNew 新密码
     * @param forgetToken
     * @return
     */
    public ServerResponse<String> forgetResetPassword(String username,String passwordNew,String forgetToken){
        if(StringUtils.isBlank(forgetToken)){
            return ServerResponse.createByErrorMessage("参数错误，token需要传递");
        }
        ServerResponse validResponse=this.checkValid(username,Const.USERNAME);
        if(validResponse.isSuccess()){
            //用户名不存在
            return ServerResponse.createByErrorMessage("用户名不存在");
        }
        String token=TokenCache.getKey(TokenCache.TOKEN_PREFIX+username);
        if(StringUtils.isBlank(token)){
            return ServerResponse.createByErrorMessage("token无效或过期");
        }
        //比较forgetToken，token成功执行修改秘密
        if(StringUtils.equals(forgetToken,token)){
            String md5password=MD5Util.MD5EncodeUtf8(passwordNew);
            int rowCont =userMapper.updatePasswordByusername(username,md5password);
            if(rowCont>0){
                return ServerResponse.createBySuccessMessage("修改密码成功");
            }
        }else{
            return ServerResponse.createByErrorMessage("token错误，请重新获取重置密码的token");
        }
        return ServerResponse.createByErrorMessage("修改密码错误");
    }

    /**
     * 登录状态下重置密码
     * @param passwordOld 旧密码
     * @param passwordNew 新密码
     * @param user user
     * @return
     */
    public ServerResponse<String> resetPassword(String passwordOld,String passwordNew,User user){
        //防止横向越权，要校验一下用户的旧密码，一定要指定是这个用户，因为我们会查询一个count（1），如果不指定id，那么结果就true,count>0
        int resultCount =userMapper.checkPassword(MD5Util.MD5EncodeUtf8(passwordOld),user.getId());
        if(resultCount==0){
            return ServerResponse.createByErrorMessage("旧密码输入错误");
        }
        user.setPassword(MD5Util.MD5EncodeUtf8(passwordNew));
        int updateCount=userMapper.updateByPrimaryKey(user);
        if(updateCount>0){
            return  ServerResponse.createBySuccessMessage("密码更新成功");
        }
        return ServerResponse.createByErrorMessage("密码更新失败");
    }


    /**
     * 更新用户个人信息
     * @param user user
     * @return
     */
    public ServerResponse<User> upateInformation(User user){
        //username是不能被更新的
        //email也要进行校验，校验一个新的email是不是应景存在，并且email已经存在的话不能是单前用户的
        int resultCount =userMapper.checkEmailByUserId(user.getId(),user.getEmail());
        if(resultCount>0){
            return  ServerResponse.createByErrorMessage("email已经存在，请更换email再尝试");
        }
        User updateUser=new User();
        updateUser.setId(user.getId());
        updateUser.setEmail(user.getEmail());
        updateUser.setPhone(user.getPhone());
        updateUser.setQuestion(user.getQuestion());
        updateUser.setAnswer(user.getAnswer());

        int updateCount=userMapper.updateByPrimaryKeySelective(updateUser);
        if(updateCount >0){
            return ServerResponse.createBySuccess("更新个人信息成功",updateUser);
        }
        return ServerResponse.createByErrorMessage("更新个人信息失败");
    }

    /**
     * 获取用户详情并把得到的密码set空
     * @param userId
     * @return
     */
    public ServerResponse<User> getInformation(Integer userId){
        User user=userMapper.selectByPrimaryKey(userId);
        if(user==null){
            return  ServerResponse.createByErrorMessage("找不到当前用户");
        }
        user.setPassword(StringUtils.EMPTY);//清空密码
        return ServerResponse.createBySuccess(user);
    }

    /**
     * 校验当前登录的用户是否为管理员
     * @param user user
     * @return
     */
    public ServerResponse chekAdminRole(User user){
        if(user!=null&&user.getRole().intValue()==Const.Role.ROLE_ADMIN){
            return ServerResponse.createBySuccess();
        }
        return ServerResponse.createByError();
    }

}
