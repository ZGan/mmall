package com.mmall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.mmall.common.Const;
import com.mmall.common.ResponseCode;
import com.mmall.common.ServerResponse;
import com.mmall.dao.CategoryMapper;
import com.mmall.dao.ProductMapper;
import com.mmall.pojo.Category;
import com.mmall.pojo.Product;
import com.mmall.service.ICategoryService;
import com.mmall.service.IProductService;
import com.mmall.util.DateTimeUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.ProductDetailVo;
import com.mmall.vo.ProductListVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品模块
 * Created by 17936 on 2017/8/19.
 */
@Service("iProductService")
public class ProductSetviceImpl implements IProductService {

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private ICategoryService iCategoryService;

    /**
     * 编辑产品信息
     * @param product
     * @return
     */
    public ServerResponse saveOrUpdateProduct(Product product){
        if(product != null)
        {
            if(StringUtils.isNotBlank(product.getSubImages())){
                String[] subImageArray = product.getSubImages().split(",");
                if(subImageArray.length > 0){
                    product.setMainImage(subImageArray[0]);
                }
            }

            if(product.getId() != null){
                int rowCount = productMapper.updateByPrimaryKey(product);
                if(rowCount > 0){
                    return ServerResponse.createBySuccess("更新产品成功");
                }
                return ServerResponse.createBySuccess("更新产品失败");
            }else{
                int rowCount = productMapper.insert(product);
                if(rowCount > 0){
                    return ServerResponse.createBySuccess("新增产品成功");
                }
                return ServerResponse.createBySuccess("新增产品失败");
            }
        }
        return ServerResponse.createByErrorMessage("新增或更新产品参数不正确");
    }

    /**
     * 商品上下架
     * @param productId id
     * @param status 销售状态
     * @return
     */
    public ServerResponse setSaleStatus(Integer productId,Integer status){
        if(productId==null||status==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product =new Product();
        product.setId(productId);
        product.setStatus(status);
        int rowCount =productMapper.updateByPrimaryKeySelective(product);
        if(rowCount>0){
            return ServerResponse.createBySuccessMessage("修改产品销售状态成功");
        }
        return ServerResponse.createByErrorMessage("修改产品销售状态失败");
    }

    /**
     * 后台查看商品详情
     * @param productId
     * @return
     */
    public ServerResponse<ProductDetailVo> manageProductDetail(Integer productId){
        if(productId==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product =productMapper.selectByPrimaryKey(productId);
        if(product==null){
            return ServerResponse.createByErrorMessage("该产品已下架或者已经删除");
        }
        //VO对象--value Object
        //prjo->>bo(business object)->>vo(view object)
        ProductDetailVo productDetailVo =assembleProductDetailVo(product);
        return ServerResponse.createBySuccess(productDetailVo);
    }

    /**
     * 动态分页（1.startPage--start  2.填充自己的sql查询逻辑  3.pageHelper-收尾）
     * @param pageNum 第几页
     * @param pageSize 一页多少 条数据
     * @return
     */
    public ServerResponse<PageInfo> getProductList(Integer pageNum,Integer pageSize ){
        //1.startPage--start
        PageHelper.startPage(pageNum,pageSize);
        // 2.填充自己的sql查询逻辑
        List<Product> productList =productMapper.selectList();

        List<ProductListVo> productListVoList = Lists.newArrayList();//装换为VO对象返回
        for(Product productItem :productList){
            ProductListVo productListVo = assembleProductListVo(productItem);
            productListVoList.add(productListVo);
        }

        //3.pageHelper-收尾
        PageInfo pageResult= new PageInfo(productListVoList);
        pageResult.setList(productListVoList);

        return  ServerResponse.createBySuccess(pageResult);
    }

    /**
     * 商品分页搜索
     * @param productName 商品名称
     * @param productId 商品id
     * @param pageNum 第几页
     * @param pageSize 一页多少 条数据
     * @return
     */
    public ServerResponse<PageInfo> searchProduct(String productName,Integer productId,int pageNum, int pageSize){
        //1.startPage--start
        PageHelper.startPage(pageNum,pageSize);
        // 2.填充自己的sql查询逻辑
        if(StringUtils.isNotBlank(productName)){
            productName =new StringBuilder().append("%").append(productName).append("%").toString();//赋值为 “%productName%” 方便模糊查询
        }
        List<Product> productList =productMapper.selectByNameAndPeoductId(productName,productId);

        List<ProductListVo> productListVoList = Lists.newArrayList();//装换为VO对象返回
        for(Product productItem :productList){
            ProductListVo productListVo = assembleProductListVo(productItem);
            productListVoList.add(productListVo);
        }
        //3.pageHelper-收尾
        PageInfo pageResult= new PageInfo(productList);
        pageResult.setList(productListVoList);
        return  ServerResponse.createBySuccess(pageResult);
    }

    /**
     * 前台查看商品详情
     * @param productId 商品ID
     * @return ProductDetailVo
     */
    public ServerResponse<ProductDetailVo> getProductDetail(Integer productId){
        if(productId==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        Product product =productMapper.selectByPrimaryKey(productId);
        if(product==null){
            return ServerResponse.createByErrorMessage("该产品已下架或者已经删除");
        }
        if(product.getStatus() != Const.ProductStatusEnum.ON_SALE.getCode()){
            return ServerResponse.createByErrorMessage("该产品已下架或者已经删除");
        }
        //VO对象--value Object
        //prjo->>bo(business object)->>vo(view object)
        ProductDetailVo productDetailVo =assembleProductDetailVo(product);
        return ServerResponse.createBySuccess(productDetailVo);
    }

    /**
     * 前台分页根据关键字和分类节点id查询并实现动态排序
     * @param keyword 关键字
     * @param categoryId 节点id
     * @param pageNum 第几页
     * @param pageSize 每页多少条
     *  @param orderBy 排序
     * @return PageInfo
     */
    public ServerResponse<PageInfo> getProductKeywordCategory(String keyword, Integer categoryId,int pageNum, int pageSize,String orderBy){
        if(StringUtils.isBlank(keyword)&&categoryId==null){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        List<Integer> categoryIdList=new ArrayList();

        if(categoryId!=null){
            Category category=categoryMapper.selectByPrimaryKey(categoryId);
            //没有该分类，并且还没有关键字，这个时候返回一个空的集合，并且不报错
            if(category==null&&StringUtils.isBlank(keyword)){
                //空的集合分页
                PageHelper.startPage(pageNum,pageSize);
                List<ProductListVo> productListVoList=Lists.newArrayList();
                PageInfo pageInfo=new PageInfo(productListVoList);
                return ServerResponse.createBySuccess(pageInfo);
            }
            //递归算法查出该节点和该节点下的子节点id
            categoryIdList=iCategoryService.selectCategoryAndChildrenById(category.getId()).getData();

        }
        if(StringUtils.isNotBlank(keyword)){
            //拼接模糊查询字段
            keyword=new StringBuilder().append("%").append(keyword).append("%").toString();
        }
        //动态排序处理
        if(StringUtils.isNotBlank(orderBy)){
            if(Const.ProductListOrderBy.PRICE_ASC_DESC.contains(orderBy)){
                //分割"_"
                String[] orderByArray=orderBy.split("_");
                //page排序是 PageHelper.orderBy("price desc")这种形式 ，要进行拼接
                PageHelper.orderBy(orderByArray[0]+" "+orderByArray[1]);
            }
        }
        List<Product> productList= productMapper.selectByNameAndCacegoryIds(StringUtils.isBlank(keyword)?null:keyword,categoryIdList.size()==0?null:categoryIdList);//根据关键字和id集合查询商品。

        List<ProductListVo> productListVoList=Lists.newArrayList();
        for (Product product : productList){
            ProductListVo productListVo=assembleProductListVo(product);
            productListVoList.add(productListVo);
        }
        PageInfo pageinfo =new PageInfo(productList);
        pageinfo.setList(productListVoList);
        return  ServerResponse.createBySuccess(pageinfo);
    }

    /**
     * VO对象接收商品详情需要的参数
     * @param product
     * @return
     */
    private ProductDetailVo assembleProductDetailVo(Product product){
        ProductDetailVo productDetailVo =new ProductDetailVo();
        productDetailVo.setId(product.getId());
        productDetailVo.setStatus(product.getStatus());
        productDetailVo.setMainImage(product.getMainImage());
        productDetailVo.setName(product.getName());
        productDetailVo.setCategoryId(product.getCategoryId());
        productDetailVo.setDetail(product.getDetail());
        productDetailVo.setSubtitle(product.getSubtitle());
        productDetailVo.setSubImages(product.getSubImages());
        productDetailVo.setPrice(product.getPrice());
        productDetailVo.setStock(product.getStock());
        productDetailVo.setSubtitle(product.getSubtitle());
        //imageHost
        productDetailVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        //parentCategoryId
        Category category =categoryMapper.selectByPrimaryKey(product.getCategoryId());
        if(category==null){
            productDetailVo.setCategoryId(0);//如果没有这个节点就设置为0默认节点
        }else{
            productDetailVo.setCategoryId(product.getCategoryId());
        }
        //createTime
        productDetailVo.setCreateTime(DateTimeUtil.dateToStr(product.getCreateTime()));
        //updateTime
        productDetailVo.setUpdateTime(DateTimeUtil.dateToStr(product.getUpdateTime()));
        return  productDetailVo;
    }

    /**
     * VO对象接收动态分页需要的参数
     * @param product
     * @return
     */
    private ProductListVo assembleProductListVo(Product product){
        ProductListVo productListVo =new ProductListVo();
        productListVo.setId(product.getId());
        productListVo.setName(product.getName());
        productListVo.setCategoryId(product.getCategoryId());
        productListVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        productListVo.setMainImage(product.getMainImage());
        productListVo.setPrice(product.getPrice());
        productListVo.setSubtitle(product.getSubtitle());
        productListVo.setStatus(product.getStatus());
        return  productListVo;
    }
}
