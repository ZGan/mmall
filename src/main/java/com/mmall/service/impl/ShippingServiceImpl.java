package com.mmall.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Maps;
import com.mmall.common.ServerResponse;
import com.mmall.dao.ShippingMapper;
import com.mmall.pojo.Shipping;
import com.mmall.service.IShippingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 收货地址模块
 * Created by 17936 on 2017/9/24.
 */
@Service("iShippingService")
public class ShippingServiceImpl implements IShippingService {

    @Autowired
    private ShippingMapper shippingMapper;

    /**
     * 新增收货地址
     * @param userId 用户编号
     * @param shipping shipping对象
     * @return
     */
    public ServerResponse add(Integer userId , Shipping shipping){
        shipping.setUserId(userId);
        int rowCount =shippingMapper.insertSelective(shipping);
        if(rowCount>0){
            Map map= Maps.newHashMap();
            map.put("shippingId",shipping.getId());
            return ServerResponse.createBySuccess("新增收货地址成功",map);
        }
        return ServerResponse.createBySuccessMessage("新增收货地址失败");
    }

    /**
     * 删除收货地址
     * @param userId 客户编号
     * @param shippingId 收货地址编号
     * @return String
     */
    public ServerResponse<String> del(Integer userId,Integer shippingId){
        int resultCount=shippingMapper.delShippingIdUserId(userId,shippingId);
        if(resultCount > 0){
            return ServerResponse.createBySuccessMessage("删除地址成功");
        }
       return ServerResponse.createByErrorMessage("删除地址失败");
    }

    /**
     * 更新收货地址
     * @param userId userId
     * @param shipping shipping对象
     * @return String
     */
    public ServerResponse<String> update(Integer userId,Shipping shipping){
        shipping.setUserId(userId);
        int rowCount=shippingMapper.updateByShipping(shipping);
        if(rowCount > 0){
            return ServerResponse.createBySuccessMessage("更新收货地址成功");
        }
        return ServerResponse.createBySuccessMessage("更新收货地址失败");
    }

    /**
     * 查询收货地址详情
     * @param userId 客户编号
     * @param shippingId 收货地址编号
     * @return Shipping
     */
    public ServerResponse<Shipping> select(Integer userId,Integer shippingId){
        Shipping shipping= shippingMapper.selectByShippingIdUserId(userId,shippingId);
        if(shipping==null){
           return ServerResponse.createByErrorMessage("无法查询到该地址");
        }
        return ServerResponse.createBySuccess("更新地址成功",shipping);
    }

    /**
     * 分页查询收货地址
     * @param  userId 用户编号
     * @param pageNum 第几页
     * @param pageSize 一页多少条
     * @return PageInfo
     */
    public ServerResponse<PageInfo> list(Integer userId,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<Shipping> shippingList = shippingMapper.selectAllByUserId(userId);
        PageInfo pageInfo = new PageInfo(shippingList);
        return ServerResponse.createBySuccess(pageInfo);
    }
}
