package com.mmall.service.impl;

import com.google.common.collect.Lists;
import com.mmall.service.IFileService;
import com.mmall.util.FTPUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件处理
 * Created by 17936 on 2017/8/22.
 */
@Service("iFileService")
public class FileServiceImpl implements IFileService {

    private Logger logger= LoggerFactory.getLogger(FileServiceImpl.class);

    /**
     * 文件上传
     * @param file 文件
     * @param path 路径
     * @return
     */
    public String upload(MultipartFile file, String path){
        //获取文件原始名
        String fileName=file.getOriginalFilename();
        //获取文件名的扩展名（abc.jpg）--> jpg'
        String fileExtensionName=fileName.substring(fileName.lastIndexOf(".")+1);
        //文件上传后名字
        // A:abc.jpg B:abc.jpg (保证名字不重复)
        String uploadFileName= UUID.randomUUID().toString()+"."+fileExtensionName;
        //打印日志
        logger.info("开始上传文件，上传的文件名为{}，上传的路径为{}，上传后文件名为{}",fileName,path,uploadFileName);
        //创建目录
        File fileDir =new File(path);
        if(!fileDir.exists()){
            fileDir.setWritable(true);//给创建权限
            fileDir.mkdirs();//创建文件
        }
        //创建文件
        File targetFile= new File(path, uploadFileName);
        try {
            file.transferTo(targetFile);//文件已近上传成功

            //将文件targetFile上传到FTP服务器上
            FTPUtil.uploadFile(Lists.newArrayList(targetFile));

            // 上传成功后删除upload下面的文件
            targetFile.delete();
        } catch (IOException e) {
            logger.error("文件上传异常",e);
            return null;
        }
        return  targetFile.getName();
    }

    public static void main(String[] args) {
        String fileName="abc.jpg";
        System.out.printf(fileName.substring(fileName.lastIndexOf(".")+1));
    }
}
