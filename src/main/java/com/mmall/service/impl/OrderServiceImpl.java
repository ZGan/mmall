package com.mmall.service.impl;

import com.alipay.api.AlipayResponse;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import com.alipay.demo.trade.config.Configs;
import com.alipay.demo.trade.model.ExtendParams;
import com.alipay.demo.trade.model.GoodsDetail;
import com.alipay.demo.trade.model.builder.AlipayTradePrecreateRequestBuilder;
import com.alipay.demo.trade.model.result.AlipayF2FPrecreateResult;
import com.alipay.demo.trade.service.AlipayMonitorService;
import com.alipay.demo.trade.service.AlipayTradeService;
import com.alipay.demo.trade.service.impl.AlipayTradeServiceImpl;
import com.alipay.demo.trade.utils.ZxingUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mmall.common.Const;
import com.mmall.common.ServerResponse;
import com.mmall.dao.*;
import com.mmall.pojo.*;
import com.mmall.service.IOrderService;
import com.mmall.util.BigDecimalUtil;
import com.mmall.util.DateTimeUtil;
import com.mmall.util.FTPUtil;
import com.mmall.util.PropertiesUtil;
import com.mmall.vo.OrderItemVo;
import com.mmall.vo.OrderProductVo;
import com.mmall.vo.OrderVo;
import com.mmall.vo.ShippingVo;
import org.apache.bcel.generic.NEW;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * 支付模块
 * Created by Administrator on 2017/9/27.
 */
@Service("iOrderService")
public class OrderServiceImpl implements IOrderService {

    private static Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderItemMapper orderItemMapper;

    @Autowired
    private PayInfoMapper payInfoMapper;

    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ShippingMapper shippingMapper;
    /**
     * 支付并生成二维码
     * @param orderNo 订单号
     * @param userId 用户编号
     * @param path 图片路径
     * @return map
     */
    public ServerResponse pay(Long orderNo,Integer userId,String path){
        Map<String ,String> resultMap= Maps.newHashMap();
        Order order= orderMapper.selectByUserIdAndOrderNo(userId,orderNo);
        if(order == null){
            return ServerResponse.createByErrorMessage("用户没有改订单");
        }
        resultMap.put("orderNo",String.valueOf(orderNo));


        // (必填) 商户网站订单系统中唯一订单号，64个字符以内，只能包含字母、数字、下划线，
        // 需保证商户系统端不能重复，建议通过数据库sequence生成，
        String outTradeNo = order.getOrderNo().toString();

        // (必填) 订单标题，粗略描述用户的支付目的。如“xxx品牌xxx门店当面付扫码消费”
        String subject = new StringBuilder().append("mmall扫码支付，订单号:").append(outTradeNo).toString();

        // (必填) 订单总金额，单位为元，不能超过1亿元
        // 如果同时传入了【打折金额】,【不可打折金额】,【订单总金额】三者,则必须满足如下条件:【订单总金额】=【打折金额】+【不可打折金额】
        String totalAmount = order.getPayment().toString();

        // (可选) 订单不可打折金额，可以配合商家平台配置折扣活动，如果酒水不参与打折，则将对应金额填写至此字段
        // 如果该值未传入,但传入了【订单总金额】,【打折金额】,则该值默认为【订单总金额】-【打折金额】
        String undiscountableAmount = "0";

        // 卖家支付宝账号ID，用于支持一个签约账号下支持打款到不同的收款账号，(打款到sellerId对应的支付宝账号)
        // 如果该字段为空，则默认为与支付宝签约的商户的PID，也就是appid对应的PID
        String sellerId = "";

        // 订单描述，可以对交易或商品进行一个详细地描述，比如填写"购买商品2件共15.00元"
        String body = new StringBuffer().append("订单").append(outTradeNo).append("购买商品共").append(totalAmount).append("元").toString();

        // 商户操作员编号，添加此参数可以为商户操作员做销售统计
        String operatorId = "test_operator_id";

        // (必填) 商户门店编号，通过门店号和商家后台可以配置精准到门店的折扣信息，详询支付宝技术支持
        String storeId = "test_store_id";

        // 业务扩展参数，目前可添加由支付宝分配的系统商编号(通过setSysServiceProviderId方法)，详情请咨询支付宝技术支持
        ExtendParams extendParams = new ExtendParams();
        extendParams.setSysServiceProviderId("2088100200300400500");

        // 支付超时，定义为120分钟
        String timeoutExpress = "120m";

        // 商品明细列表，需填写购买商品详细信息，
        List<GoodsDetail> goodsDetailList = new ArrayList<GoodsDetail>();

        List<OrderItem> orderItemList=orderItemMapper.getByOrderNoUserId(orderNo,userId);

        // 创建一个商品信息，参数含义分别为商品id（使用国标）、名称、单价（单位为分）、数量，如果需要添加商品类别，详见GoodsDetail
        for (OrderItem orderItem:orderItemList){
            // 创建一个商品信息，参数含义分别为商品id（使用国标）、名称、单价（单位为分）、数量，如果需要添加商品类别，详见GoodsDetail
            GoodsDetail goods1 = GoodsDetail.newInstance(orderItem.getProductId().toString(), orderItem.getProductName(),
                    BigDecimalUtil.mul(orderItem.getCurrentUnitPrice().doubleValue(),new Double(100).doubleValue()).longValue(),
                    orderItem.getQuantity());
            goodsDetailList.add(goods1);
        }

        // 创建扫码支付请求builder，设置请求参数
        AlipayTradePrecreateRequestBuilder builder = new AlipayTradePrecreateRequestBuilder()
                .setSubject(subject).setTotalAmount(totalAmount).setOutTradeNo(outTradeNo)
                .setUndiscountableAmount(undiscountableAmount).setSellerId(sellerId).setBody(body)
                .setOperatorId(operatorId).setStoreId(storeId).setExtendParams(extendParams)
                .setTimeoutExpress(timeoutExpress)
                .setNotifyUrl(PropertiesUtil.getProperty("alipay.callback.url"))//支付宝服务器主动通知商户服务器里指定的页面http路径,根据需要设置
                .setGoodsDetailList(goodsDetailList);


        /** 一定要在创建AlipayTradeService之前调用Configs.init()设置默认参数
         *  Configs会读取classpath下的zfbinfo.properties文件配置信息，如果找不到该文件则确认该文件是否在classpath目录
         */
        Configs.init("zfbinfo.properties");

        /** 使用Configs提供的默认参数
         *  AlipayTradeService可以使用单例或者为静态成员对象，不需要反复new
         */
        AlipayTradeService tradeService = new AlipayTradeServiceImpl.ClientBuilder().build();

        AlipayF2FPrecreateResult result = tradeService.tradePrecreate(builder);

        switch (result.getTradeStatus()) {
            case SUCCESS:
                log.info("支付宝预下单成功: )");

                AlipayTradePrecreateResponse response = result.getResponse();
                dumpResponse(response);

                File folder=new File(path);
                //如果目录不存在
                if(!folder.exists()){
                    folder.setWritable(true);//给写权限
                    folder.mkdir();//创建目录
                }

                // 需要修改为运行机器上的路径
                String qrPath = String.format(path+"/qr-%s.png", response.getOutTradeNo());
                //生成文件
                String qrFileName= String.format("qr-%s.png",response.getOutTradeNo());
                ZxingUtils.getQRCodeImge(response.getQrCode(), 256, qrPath);

                //传输到文件服务器上
                File targetFile=new File(path,qrFileName);
                try {
                    FTPUtil.uploadFile(Lists.newArrayList(targetFile));
                } catch (IOException e) {
                    log.error("上传二维码异常",e);
                }
                log.info("qrPath:" + qrPath);
                String qrUrl=PropertiesUtil.getProperty("ftp.server.http.prefix")+targetFile.getName();
                resultMap.put("qrUrl",qrUrl);
                return  ServerResponse.createBySuccess(resultMap);

            case FAILED:
                log.error("支付宝预下单失败!!!");
               return  ServerResponse.createByErrorMessage("支付宝预下单失败!!!");

            case UNKNOWN:
                log.error("系统异常，预下单状态未知!!!");
                return  ServerResponse.createByErrorMessage("系统异常，预下单状态未知!!!");

            default:
                log.error("不支持的交易状态，交易返回异常!!!");
                return  ServerResponse.createByErrorMessage("不支持的交易状态，交易返回异常!!!");
        }
    }

    // 简单打印应答
    private void dumpResponse(AlipayResponse response) {
        if (response != null) {
            log.info(String.format("code:%s, msg:%s", response.getCode(), response.getMsg()));
            if (StringUtils.isNotEmpty(response.getSubCode())) {
                log.info(String.format("subCode:%s, subMsg:%s", response.getSubCode(),
                        response.getSubMsg()));
            }
            log.info("body:" + response.getBody());
            System.out.printf("body:"+response.getBody());
        }
    }

    /**
     * 回调本地校验
     * @param params 支付宝回调参数
     * @return ServerResponse
     */
    public ServerResponse aliCallback(Map<String,String> params){
        Long orderNo =Long.parseLong(params.get("out_trade_no"));//拿到订单号 out_trade_no
        String tradeNo=params.get("trade_no");//拿到支付宝的交易号 trade_no
        String tradeStatus=params.get("trade_status");//拿到交易状态 trade_status

        Order order=orderMapper.selectByOrderNo(orderNo);
        if(order == null){
            return ServerResponse.createByErrorMessage("不是该商场的订单，回调结果忽略");
        }
        if(order.getStatus() >= Const.OrderStatusEnum.PAID.getCode()){
            return  ServerResponse.createBySuccess("支付宝重复调用");
        }
        if(Const.AlipayCallback.TRADE_STATUS_TRADE_SUCCESS.equals(tradeStatus)){
            //交易成功
            order.setPaymentTime(DateTimeUtil.strToDate(params.get("gmt_payment")));//订单付款时间
            order.setStatus(Const.OrderStatusEnum.PAID.getCode());//交易状态
            orderMapper.updateByPrimaryKeySelective(order);
        }
        PayInfo payInfo =new PayInfo();
        payInfo.setUserId(order.getUserId());//用户编号
        payInfo.setOrderNo(order.getOrderNo());//订单号
        payInfo.setPayPlatform(Const.PayPlatformEnum.ALIPAY.getCode());//支付方式
        payInfo.setPlatformNumber(tradeNo);//交易号
        payInfo.setPlatformStatus(tradeStatus);//交易状态
        payInfoMapper.insertSelective(payInfo);

        return ServerResponse.createBySuccess();
    }

    /**
     * 查询订单是否支付成功
     * @param userId 用户编号
     * @param orderNo 订单号
     * @return Boolean
     */
    public  ServerResponse<Boolean> queryOderPayStatus(Integer userId,Long orderNo){
        Order order =orderMapper.selectByUserIdAndOrderNo(userId, orderNo);
        if(order== null){
            return  ServerResponse.createBySuccessMessage("没有该订单");
        }
        //判断支付状态
        if(order.getStatus() >= Const.OrderStatusEnum.PAID.getCode()){
            return ServerResponse.createBySuccess();
        }
        return  ServerResponse.createByError();
    }

    /**
     * 增加订单
     * @param userId 用户编号
     * @param shippingId 收货地址编号
     * @return
     */
    public ServerResponse createOrder(Integer userId,Integer shippingId){
        //从购物车中获取已勾选的数据
        List<Cart> cartList=cartMapper.selectChekedCartByUserId(userId);

        ServerResponse serverResponse=this.getCartOrderItem(userId,cartList);//根据购物车对象，创建子订单明细

        if(!serverResponse.isSuccess()){
            return serverResponse;
        }
        List<OrderItem> orderItemList =(List<OrderItem>)serverResponse.getData();

        BigDecimal payment=this.getOrderTotalPrice(orderItemList);//计算这个订单的总价

        if( CollectionUtils.isEmpty(orderItemList)){
            return  ServerResponse.createByErrorMessage("购物车为空");
        }

        //生成订单
        Order order =this.assembleOrder(userId,shippingId,payment);

        if(order == null){
            return  ServerResponse.createByErrorMessage("生成订单错误");
        }
        if(CollectionUtils.isEmpty(orderItemList)){
            return  ServerResponse.createByErrorMessage("购物车为空");
        }

        for(OrderItem  orderItem:orderItemList){
            orderItem.setOrderNo(order.getOrderNo());
        }

        orderItemMapper.batchInsert(orderItemList);//mybatis 批量插入

        this.reduceProductStock(orderItemList);//生成成功，减少产品库存

        this.cleanCart(cartList);//清空购物车

        //返回给前端数据
        OrderVo orderVo = assembleOrderVo(order,orderItemList);
        return ServerResponse.createBySuccess(orderVo);
    }

    /**
     * 取消订单
     * @param userId userId
     * @param orderNo 订单号
     * @return String
     */
    public ServerResponse<String> cancel(Integer userId,Long orderNo){
        Order order =orderMapper.selectByUserIdAndOrderNo(userId,orderNo);
        if(order == null){
            return  ServerResponse.createBySuccessMessage("该用户不存在该订单");
        }
        if(order.getStatus() != Const.OrderStatusEnum.NO_PAY.getCode()){
            return  ServerResponse.createBySuccessMessage("已付款，无法取消订单");
        }
        Order updateOrder =new Order();
        updateOrder.setId(order.getId());
        updateOrder.setStatus(Const.OrderStatusEnum.CANCELED.getCode());
        int row =orderMapper.updateByPrimaryKeySelective(updateOrder);
        if(row > 0){
            return  ServerResponse.createBySuccessMessage("取消订单成功");
        }
        return  ServerResponse.createByError();
    }

    /**
     * 获取选中购物车中商品信息
     * @param userId
     * @return
     */
    public ServerResponse getOrderCartProduct(Integer userId){
        OrderProductVo orderProductVo =new OrderProductVo();
        //从购物车里面获取数据
        List<Cart> cartList =cartMapper.selectChekedCartByUserId(userId);
        ServerResponse serverResponse =this.getCartOrderItem(userId,cartList);
        if(!serverResponse.isSuccess()){
            return serverResponse;
        }
        List<OrderItem> orderItemList = (List<OrderItem>)serverResponse.getData();

        List<OrderItemVo> orderItemVoList =Lists.newArrayList();

        BigDecimal payment = new BigDecimal("0");
        for (OrderItem orderItem : orderItemList){
            payment =BigDecimalUtil.add(payment.doubleValue(),orderItem.getTotalPrice().doubleValue());
            orderItemVoList.add(assembleOrderItemVo(orderItem));//订单明细
        }
        orderProductVo.setProductTotalPrice(payment);
        orderProductVo.setOrderitemVoList(orderItemVoList);
        orderProductVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));
        return ServerResponse.createBySuccess(orderProductVo);
    }

    /**
     * 订单详情
     * @param userId 用户编号
     * @param orderNo 订单号
     * @return ServerResponse
     */
    public ServerResponse getOrderDetail(Integer userId,Long orderNo){
        Order order =orderMapper.selectByUserIdAndOrderNo(userId,orderNo);
        if(order != null){
            List<OrderItem> orderItemList =orderItemMapper.getByOrderNoUserId(orderNo, userId);
            OrderVo orderVo =assembleOrderVo(order,orderItemList);
            return ServerResponse.createBySuccess(orderVo);
        }
        return ServerResponse.createByErrorMessage("没有找到该订单");
    }

    /**
     * 订单list
     * @param userId userId
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return PageInfo
     */
    public ServerResponse<PageInfo> getOrderList(Integer userId,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderList=orderMapper.selectByUserId(userId);
        List<OrderVo> orderVoList= assembleOrderVoList(orderList,userId);
        PageInfo pageInfo =new PageInfo(orderList);
        pageInfo.setList(orderVoList);
        return ServerResponse.createBySuccess(pageInfo);
    }

    /**
     * 订单list明细
     * @param orderList 订单
     * @param userId userId
     * @return OrderVo
     */
    private List<OrderVo> assembleOrderVoList(List<Order> orderList,Integer userId){
        List<OrderVo> orderVoList =Lists.newArrayList();
        for(Order order :orderList){
            List<OrderItem> orderItemList=Lists.newArrayList();
            if(userId == null){
                // 管理员登录的时候不需要传用户编号
                orderItemList=orderItemMapper.getByOrderNo(order.getOrderNo());
            }else{
                orderItemList=orderItemMapper.getByOrderNoUserId(order.getOrderNo(),userId);
            }
            OrderVo orderVo =assembleOrderVo(order,orderItemList);
            orderVoList.add(orderVo);
        }
        return  orderVoList;
    }

    /**
     * 增加订单返回给前端数据(OrderVo)
     * @param order
     * @param orderItemList
     * @return
     */
    private OrderVo assembleOrderVo(Order order ,List<OrderItem> orderItemList){
        OrderVo orderVo =new OrderVo();
        orderVo.setOrderNo(order.getOrderNo());
        orderVo.setPayment(order.getPayment());
        orderVo.setPaymentType(order.getPaymentType());
        orderVo.setPaymentTypeDesc(Const.PaymentTypeEnum.codeOf(order.getPaymentType()).getValue());

        orderVo.setPostage(order.getPostage());
        orderVo.setStatus(order.getStatus());
        orderVo.setStatusDesc(Const.OrderStatusEnum.codeOf(order.getStatus()).getValue());

        orderVo.setShippingId(order.getShippingId());
        Shipping shipping=shippingMapper.selectByPrimaryKey(order.getShippingId());
        if(shipping!=null){
            orderVo.setShippingName(shipping.getReceiverName());
            //组装shippingVo
            orderVo.setShippingVoList(assembleShippingVo(shipping));
        }
        orderVo.setPaymentTime(DateTimeUtil.dateToStr(order.getPaymentTime()));
        orderVo.setSendTime(DateTimeUtil.dateToStr(order.getSendTime()));
        orderVo.setEndTime(DateTimeUtil.dateToStr(order.getEndTime()));
        orderVo.setCreateTime(DateTimeUtil.dateToStr(order.getCreateTime()));
        orderVo.setCloseTime(DateTimeUtil.dateToStr(order.getCloseTime()));


        orderVo.setImageHost(PropertiesUtil.getProperty("ftp.server.http.prefix"));


        List<OrderItemVo> orderItemVoList = Lists.newArrayList();

        for(OrderItem orderItem : orderItemList){
            OrderItemVo orderItemVo = assembleOrderItemVo(orderItem);
            orderItemVoList.add(orderItemVo);
        }
        orderVo.setOrderItemVoList(orderItemVoList);
        return orderVo;
    }

    /**
     * 组装 ShippingVo
     * @param shipping
     * @return
     */
    private ShippingVo assembleShippingVo(Shipping shipping){
        ShippingVo shippingVo = new ShippingVo();
        shippingVo.setReceiverName(shipping.getReceiverName());
        shippingVo.setReceiverAddress(shipping.getReceiverAddress());
        shippingVo.setReceiverProvince(shipping.getReceiverProvince());
        shippingVo.setReceiverCity(shipping.getReceiverCity());
        shippingVo.setReceiverDistrict(shipping.getReceiverDistrict());
        shippingVo.setReceiverMobile(shipping.getReceiverMobile());
        shippingVo.setReceiverZip(shipping.getReceiverZip());
        shippingVo.setReceiverPhone(shippingVo.getReceiverPhone());
        return shippingVo;
    }

    /**
     * 组装订单明细VO
     * @param orderItem
     * @return
     */
    private OrderItemVo assembleOrderItemVo(OrderItem orderItem){
        OrderItemVo orderItemVo = new OrderItemVo();
        orderItemVo.setOrderNo(orderItem.getOrderNo());
        orderItemVo.setProductId(orderItem.getProductId());
        orderItemVo.setProductName(orderItem.getProductName());
        orderItemVo.setProductImage(orderItem.getProductImage());
        orderItemVo.setCurrentUnitPrice(orderItem.getCurrentUnitPrice());
        orderItemVo.setQuantity(orderItem.getQuantity());
        orderItemVo.setTotalPrice(orderItem.getTotalPrice());
        orderItemVo.setCreateTime(DateTimeUtil.dateToStr(orderItem.getCreateTime()));
        return orderItemVo;
    }

    /**
     * 清空购物车
     * @param cartList
     */
    private void cleanCart(List<Cart> cartList){
        for(Cart cart:cartList){
            cartMapper.deleteByPrimaryKey(cart.getId());
        }
    }

    /**
     * 生成订单成功，要减少产品库存
     * @param orderItemList
     */
    private void reduceProductStock(List<OrderItem> orderItemList){
        for(OrderItem orderItem:orderItemList){
            Product product =productMapper.selectByPrimaryKey(orderItem.getProductId());
            product.setStock(product.getStock()-orderItem.getQuantity());
            productMapper.updateByPrimaryKeySelective(product);
        }
    }

    /**
     * 生成订单
     * @param userId 用户编号
     * @param shippingId 收货地址编号
     * @param payment 总价
     * @return Order
     */
    private Order assembleOrder(Integer userId,Integer shippingId,BigDecimal payment){
        Order order =new Order();
        long orderNo=this.generateOrderNo();

        order.setOrderNo(orderNo);//订单号

        order.setStatus(Const.OrderStatusEnum.NO_PAY.getCode());//状态（未付款）
        order.setPostage(0);//运费（扩展用默认设置为零）
        order.setPaymentType(Const.PaymentTypeEnum.ONLINE_PAY.getCode());//在线支付
        order.setPayment(payment);//总价格

        order.setUserId(userId);//用户编号
        order.setShippingId(shippingId);//收货地址编号

        int rowCount=orderMapper.insert(order);//生成订单
        if(rowCount>0){
            return order;
        }
        return null;
    }

    /**
     * 生成订单号
     * @return long
     */
    private long generateOrderNo(){
        long currentTime=System.currentTimeMillis();
        return  currentTime+new Random().nextInt(100);
    }

    /**
     * 计算总价
     * @param orderItemList
     * @return BigDecimal
     */
    private BigDecimal getOrderTotalPrice(List<OrderItem> orderItemList){
        BigDecimal payment=new BigDecimal("0");
        for(OrderItem  orderItem : orderItemList){
            payment=BigDecimalUtil.add(payment.doubleValue(),orderItem.getTotalPrice().doubleValue());
        }
        return payment;
    }

    /**
     * 根据购物车对象，创建子订单明细
     * @param userId 用户编号
     * @param cartList 购物车
     * @return List<OrderItem>
     */
    private ServerResponse getCartOrderItem(Integer userId,List<Cart> cartList){
        List<OrderItem> orderItemList=Lists.newArrayList();
        if(CollectionUtils.isEmpty(cartList)){
            return ServerResponse.createByErrorMessage("购物车为空");
        }
        //校验购物车的数据，包括产品和数量
        for(Cart cartItem : cartList){
            OrderItem orderItem =new OrderItem();
            Product product=productMapper.selectByPrimaryKey(cartItem.getProductId());
            //校验是否在线
            if(Const.ProductStatusEnum.ON_SALE.getCode() != product.getStatus()){
                //产品不在线（不在售卖状态）
                return  ServerResponse.createBySuccessMessage("产品"+product.getName()+"不是在售卖状态");
            }
            //校验库存
            if(cartItem.getQuantity()>product.getStock()){
                return ServerResponse.createByErrorMessage("产品"+product.getName()+"库存不足");
            }
            //组装orderItem
            orderItem.setUserId(userId);//用户编号
            orderItem.setProductId(product.getId());//产品编号
            orderItem.setProductName(product.getName());//产品名字
            orderItem.setProductImage(product.getMainImage());//产品主图
            orderItem.setCurrentUnitPrice(product.getPrice());//价格快照
            orderItem.setQuantity(cartItem.getQuantity());//数量
            orderItem.setTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(),cartItem.getQuantity()));//总价
            orderItemList.add(orderItem);
        }
        return ServerResponse.createBySuccess(orderItemList);
    }
    //backend

    /**
     * 后台订单列表
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return PageInfo
     */
    public ServerResponse<PageInfo> manageList(int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        List<Order> orderList = orderMapper.selectOrder();
        List<OrderVo> orderVoList = this.assembleOrderVoList(orderList,null);
        PageInfo pageResult = new PageInfo(orderList);
        pageResult.setList(orderVoList);
        return ServerResponse.createBySuccess(pageResult);
    }

    /**
     * 后台订单详情
     * @param orderNo 订单号
     * @return orderVo
     */
    public ServerResponse manageDetail(Long orderNo){
        Order order =orderMapper.selectByOrderNo(orderNo);
        if(order!=null){
            List<OrderItem> orderItemList=orderItemMapper.getByOrderNo(orderNo);
            OrderVo orderVo =this.assembleOrderVo(order,orderItemList);
            return ServerResponse.createBySuccess(orderVo);
        }
        return ServerResponse.createByErrorMessage("订单不存在");
    }

    /**
     * 根据订单号后台搜索
     * @param orderNo orderNo
     * @param pageNum pageNum
     * @param pageSize pageSize
     * @return PageInfo
     */
    public ServerResponse<PageInfo> manageSearch(Long orderNo,int pageNum,int pageSize){
        PageHelper.startPage(pageNum,pageSize);
        Order order =orderMapper.selectByOrderNo(orderNo);
        if(order!=null){
            List<OrderItem> orderItemList=orderItemMapper.getByOrderNo(orderNo);
            OrderVo orderVo =this.assembleOrderVo(order,orderItemList);
            PageInfo pageInfo=new PageInfo(Lists.newArrayList(order));
            pageInfo.setList(Lists.newArrayList(orderVo));
            return ServerResponse.createBySuccess(pageInfo);
        }
        return ServerResponse.createByErrorMessage("订单不存在");
    }

    /**
     * 订单发货
     * @param orderNo 订单号
     * @return String
     */
    public ServerResponse<String> orderSendGoods(Long orderNo){
        Order order=orderMapper.selectByOrderNo(orderNo);
        if(order != null){
            if(order.getStatus() ==Const.OrderStatusEnum.PAID.getCode()){
                order.setStatus(Const.OrderStatusEnum.SHIPPED.getCode());
                order.setSendTime(new Date());
                int row =orderMapper.updateByPrimaryKey(order);
                if(row >0){
                    return ServerResponse.createBySuccessMessage("发货成功");
                }
                return ServerResponse.createByErrorMessage("发货失败");
            }
            return ServerResponse.createByErrorMessage("该订单未付款，不能发货");
        }
        return ServerResponse.createByErrorMessage("订单不存在");
    }
}
