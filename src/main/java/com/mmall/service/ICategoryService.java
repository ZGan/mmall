package com.mmall.service;

import com.mmall.common.ServerResponse;
import com.mmall.pojo.Category;

import java.util.List;

/**
 * 商品品类
 * Created by 17936 on 2017/8/18.
 */
public interface ICategoryService {

    ServerResponse<String> addCategory(String categoryName, Integer pareentId);//添加商品品类

    ServerResponse<String> upcateCategoryName(String categoryName,Integer categoryId);//更新商品品类名称

    ServerResponse<List<Category>> getChildrenParallelCategory(Integer categoryId);//查询子节点信息，并且不递归，保持平级

    ServerResponse<List<Integer>> selectCategoryAndChildrenById(Integer categoryId);//递归查询当前节点的id以及孩子节点的id


}
