package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Shipping;

/**
 * 收货地址模块
 * Created by 17936 on 2017/9/24.
 */
public interface IShippingService {

    ServerResponse add(Integer userId , Shipping shipping);//新增收货地址

    ServerResponse<String> del(Integer userId,Integer shippingId);//删除收货地址

    ServerResponse<String> update(Integer userId,Shipping shipping);//更新收货地址

    ServerResponse<Shipping> select(Integer userId,Integer shippingId);//查询收货地址详情

    ServerResponse<PageInfo> list(Integer userId, int pageNum, int pageSize);//分页查询收货地址
}
