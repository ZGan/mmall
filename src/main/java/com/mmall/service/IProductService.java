package com.mmall.service;

import com.github.pagehelper.PageInfo;
import com.mmall.common.ServerResponse;
import com.mmall.pojo.Product;
import com.mmall.vo.ProductDetailVo;

/**
 * 商品模块
 * Created by 17936 on 2017/8/19.
 */
public interface IProductService {

    ServerResponse saveOrUpdateProduct(Product product);//编辑产品信息

    ServerResponse setSaleStatus(Integer productId,Integer status);//产品上下架

    ServerResponse<ProductDetailVo> manageProductDetail(Integer productId);//前台查看商品详情

    ServerResponse<PageInfo> getProductList(Integer pageNum, Integer pageSize );//动态分页

    ServerResponse<PageInfo> searchProduct(String productName,Integer productId,int pageNum, int pageSize);//产品分页搜索

    ServerResponse<ProductDetailVo> getProductDetail(Integer productId);//后台查看商品详情

    ServerResponse<PageInfo> getProductKeywordCategory(String keyword, Integer categoryId,int pageNum, int pageSize,String orderBy);//前台分页根据关键字和分类节点id查询并实现动态排序

}
